import React from 'react';
import { Icon } from '@ui-kitten/components';

export const EyeIcon = (style) => (
  <Icon {...style} name='eye' />
);

export const EyeOffIcon = (style) => (
  <Icon {...style} name='eye-off' />
);

export const PersonIcon = (style) => (
  <Icon {...style} name='person' />
);

export const EmailIcon = (style) => (
  <Icon {...style} name='email' />
);

export const PlusIcon = (style) => (
  <Icon {...style} name='plus' />
);

export const SignInIcon = (style) => (
  <Icon {...style} name='log-in-outline' />
);

export const SignUpIcon = (style) => (
  <Icon {...style} name='credit-card-outline' />
);

export const heartIcon = (style) => (
  <Icon {...style} name='heart-outline' />
);

export const clickHeartIcon = (style) => (
  <Icon {...style} name='heart' />
)

export const messageCircleIcon = (style) => (
  <Icon {...style} name='message-circle-outline' />
);

export const backIcon = (style) => (
  <Icon {...style} name='arrow-ios-back-outline' />
);

export const bookMarkIcon = (style) => (
  <Icon {...style} name='bookmark-outline' />
);

export const addIcon = (style) => (
  <Icon {...style} name='plus-circle-outline' />
);

export const shareIcon = (style) => (
  <Icon {...style} name='share' />
);

export const cameraIcon = (style) => (
  <Icon {...style} name='camera' />
);

export const editIcon = (style) => (
  <Icon {...style} name='edit-2-outline' />
);

export const moreIcon = (style) => (
  <Icon {...style} name='more-vertical-outline' />
);

export const searchIcon = (style) => (
  <Icon {...style} name='search' fill='black' />
);