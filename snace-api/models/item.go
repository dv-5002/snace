package models

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Item struct {
	gorm.Model
	ItemName    string `gorm:"not null", json:"itemName"`
	Description string `gorm:"not null", json:"description"`
	Serve       string `gorm:"not null", json:"serve"`
	Ingredient  string `gorm:"not null", json:"ingredient"`
	Method      string `gorm:"not null", json:"method"`
	UserID      uint   `gorm:"not null", json:"userId"`
	Image       string `json:"image"`
}

type ItemGorm struct {
	db *gorm.DB
}

type ItemService interface {
	CreateItem(itemReq *Item) error
	GetItemsList(userID uint) ([]Item, error)
	GetItemById(itemID uint) (*Item, error)
	DeleteItem(itemID uint, userID uint) error
	UpdateItem(itemReq *Item, itemID uint) error
	GetItemsDeletedByItemID(itemID uint) (*Item, error)
}

func NewItemService(db *gorm.DB) ItemService {
	return &ItemGorm{db}
}

func (itemg *ItemGorm) CreateItem(itemReq *Item) error {
	return itemg.db.Create(&itemReq).Error
}

func (itemg *ItemGorm) GetItemsList(userID uint) ([]Item, error) {
	items := []Item{}
	if err := itemg.db.Order("created_at DESC").Where("user_id = ?", userID).Find(&items).Error; err != nil {
		return nil, err
	}

	return items, nil
}

func (itemg *ItemGorm) GetItemById(itemID uint) (*Item, error) {
	item := Item{}
	if err := itemg.db.Where("id = ?", itemID).First(&item).Error; err != nil {
		return nil, errors.New("Item is not found")
	}
	return &item, nil
}

func (itemg *ItemGorm) DeleteItem(itemID uint, userID uint) error {
	item, err := itemg.GetItemById(itemID)
	if err != nil {
		return err
	}

	if item.UserID != userID {
		return errors.New("user id is not match")
	}

	if err := itemg.db.Delete(&item).Error; err != nil {
		return errors.New("Delete item is not success")
	}
	return nil
}

func (itemg *ItemGorm) UpdateItem(itemReq *Item, itemID uint) error {
	item, err := itemg.GetItemById(itemID)
	if err != nil {
		return err
	}

	if itemReq.ItemName != "" {
		item.ItemName = itemReq.ItemName
	}

	if itemReq.Description == "" || itemReq.Description != "" {
		item.Description = itemReq.Description
	}

	if itemReq.Serve >= "" {
		item.Serve = itemReq.Serve
	}

	if itemReq.Ingredient >= "" {
		item.Ingredient = itemReq.Ingredient
	}

	if itemReq.Method != "" {
		item.Method = itemReq.Method
	}

	if err := itemg.db.Save(&item).Error; err != nil {
		return err
	}
	return nil
}

func (itemg *ItemGorm) GetItemsDeletedByItemID(itemID uint) (*Item, error) {
	item := Item{}
	if err := itemg.db.Unscoped().Where("id = ?", itemID).First(&item).Error; err != nil {
		return nil, errors.New("Item is not found")
	}

	// image := ItemImage{}
	// err := itemg.db.Where("item_id = ?", item.ID).First(&image).Error
	// if err != nil {
	// 	item.Image = image
	// }
	// item.Image = image

	return &item, nil
}
