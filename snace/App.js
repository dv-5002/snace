import React from "react"
import { StatusBar } from 'react-native'
import { EvaIconsPack } from "@ui-kitten/eva-icons"
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components'
import { mapping, light as lightTheme } from '@eva-design/eva'
import { StackRouter } from "./src/routes/StackRouter"
import { default as appTheme } from './custom-theme.json'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore, persistReducer } from 'redux-persist'
import { createStore } from 'redux'
import AsyncStorage from '@react-native-community/async-storage'
import { rootReducer } from './src/reducers'

const App = () => {
  const theme = { ...lightTheme, ...appTheme }

  const persistConfig = {
    key: 'root',
    storage: AsyncStorage
  }
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const appStore = createStore(persistedReducer);
  const persistor = persistStore(appStore);

  return (
    <>
      <Provider store={appStore}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar translucent backgroundColor="transparent" barStyle='white-content' />
          <IconRegistry icons={EvaIconsPack} />
          <ApplicationProvider mapping={mapping} theme={theme}>
            <StackRouter />
          </ApplicationProvider>
        </PersistGate>
      </Provider>
    </>
  )
}

export default App