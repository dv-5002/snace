package middleware

import (
	"final-project-backend/context"
	"final-project-backend/header"
	"final-project-backend/models"

	"github.com/gin-gonic/gin"
)

func RequireUser(us models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := header.GetToken(c)
		if token == "" {
			c.Status(401)
			c.Abort()
			return
		}
		user, err := us.GetUserByToken(token)
		if err != nil {
			c.Status(401)
			c.Abort()
			return
		}

		context.SetUser(c, user)
	}
}
