import axios from 'axios';

export const host = "http://10.80.126.45:8080";

export function login(loginInfo) {
    return axios.post(`${host}/auth/login`, { email: loginInfo.email, password: loginInfo.password })
}

export function getItemsList(token) {
    return axios.get(`${host}/items`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function getItemInfo(itemId, token) {
    return axios.get(`${host}/item/${itemId}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function updateItemInfo(formValue, itemInfo, itemId, token) {
    let itemName = formValue.itemName;
    let description = formValue.description;
    let serve = formValue.serve;
    let ingredient = formValue.ingredient;
    let method = formValue.method;

    return axios.patch(`${host}/item/${itemId}/update`, {
        itemName: itemName,
        description: description,
        serve: serve,
        ingredient: ingredient,
        method: method,
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function removeItem(itemId, token) {
    return axios.delete(`${host}/item/${itemId}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function addNewItem(form, itemInfo, token) {
    let itemName = form.itemName;
    let description = form.description;
    let serve = form.serve;
    let ingredient = form.ingredient;
    let method = form.method;

    return axios.post(`${host}/item`, {
        itemName: itemName,
        description: description,
        serve: serve,
        ingredient: ingredient,
        method: method,
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function uploadImage(itemId, image, token) {
    const form = new FormData();
    form.append("image", {
        uri: image.uri,
        name: image.fileName,
        type: image.type,
    })

    return axios.post(`${host}/item/${itemId}/image`, form, {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": 'multipart/form-data'
        }
    });
}

export function getUserProfile(token) {
    return axios.get(`${host}/user/profile`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function logout(token) {
    return axios.post(`${host}/user/logout`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    }).then((res) => {
        console.log(res)
    })
}

export function checkEmailAvailability(email) {
    return axios.get(`${host}/auth/checkEmailAvailability?email=${email}`)
}

export function updateUserProfile(form, userInfo, token) {
    return axios.patch(`${host}/user/profile`, { email: userInfo.email, firstname: form.firstname, lastname: form.lastname }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function uploadUserImage(image, token) {
    const form = new FormData();
    form.append("userImage", {
        uri: image.uri,
        name: image.fileName,
        type: image.type,
    })

    return axios.patch(`${host}/user/uploadimage`, form, {
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": 'multipart/form-data'
        }
    });
}

export function registerNewUser(form) {
    return axios.post(`${host}/auth/signup`, form)
}