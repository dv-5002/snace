package handlers

import (
	"errors"
	"final-project-backend/context"
	"final-project-backend/models"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ItemImageHandler struct {
	itemImgs models.ItemImageService
	items    models.ItemService
}

type ItemImageRes struct {
	ID       uint   `json:"id"`
	itemID   uint   `json:"itemId"`
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

type CreateImageRes struct {
	ItemImageRes
}

func NewItemImageHandler(itemImgs models.ItemImageService, items models.ItemService) *ItemImageHandler {
	return &ItemImageHandler{itemImgs, items}
}

func (itemImgh *ItemImageHandler) CreateItemImage(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	item, err := itemImgh.items.GetItemById(uint(itemID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	if item.UserID != user.ID {
		Error(c, 401, errors.New("You cannot upload image this item"))
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		Error(c, 400, err)
		return
	}

	imageUpload, err := itemImgh.itemImgs.CreateImage(form.File["image"][0], uint(itemID))
	if err != nil {
		Error(c, 500, err)
		return
	}

	res := new(CreateImageRes)
	res.ID = imageUpload.ID
	res.Filename = filepath.Join(models.UploadPath, itemIDStr, imageUpload.Filename)
	res.Url = imageUpload.Url

	c.JSON(201, res)
}
