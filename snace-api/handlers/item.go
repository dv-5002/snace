package handlers

import (
	"final-project-backend/context"
	"final-project-backend/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ItemHandler struct {
	its        models.ItemService
	itemImages models.ItemImageService
}

type NewItem struct {
	ItemName    string `json:"itemName"`
	Description string `json:"description"`
	Serve       string `json:"serve"`
	Ingredient  string `json:"ingredient"`
	Method      string `json:"method"`
	UserID      uint   `json:"userId"`
}

type ItemRes struct {
	ID          uint         `json:"id"`
	ItemName    string       `json:"itemName"`
	Description string       `json:"description"`
	Serve       string       `json:"serve"`
	Ingredient  string       `json:"ingredient"`
	Method      string       `json:"method"`
	UserID      uint         `json:"userId"`
	Image       ItemImageRes `json:"image"`
}

type ItemListRes struct {
	ID uint `json:"id"`
	ItemRes
}

type ItemResponse struct {
	ID          uint         `json:"id"`
	ItemName    string       `json:"itemName"`
	Description string       `json:"description"`
	Serve       string       `json:"serve"`
	Ingredient  string       `json:"ingredient"`
	Method      string       `json:"method"`
	UserID      uint         `json:"userId"`
	Image       ItemImageRes `json:"image"`
}

type ItemUpdate struct {
	ItemName    string `json:"itemName"`
	Description string `json:"description"`
	Serve       string `json:"serve"`
	Ingredient  string `json:"ingredient"`
	Method      string `json:"method"`
}

type UpdateItemSelected struct {
	IsSelected bool `json:"isSelected"`
}

func NewItemHandler(its models.ItemService, itemImages models.ItemImageService) *ItemHandler {
	return &ItemHandler{its, itemImages}
}

func (ith *ItemHandler) CreateNewItem(c *gin.Context) {
	newItem := new(NewItem)
	if err := c.BindJSON(newItem); err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	item := new(models.Item)
	item.ItemName = newItem.ItemName
	item.Description = newItem.Description
	item.Serve = newItem.Serve
	item.Ingredient = newItem.Ingredient
	item.Method = newItem.Method
	item.UserID = user.ID
	if err := ith.its.CreateItem(item); err != nil {
		Error(c, 500, err)
		return
	}

	itemResponse := ItemRes{
		ID:          item.ID,
		ItemName:    item.ItemName,
		Description: item.Description,
		Serve:       item.Serve,
		Ingredient:  item.Ingredient,
		Method:      item.Method,
		UserID:      item.UserID,
	}

	c.JSON(200, itemResponse)
}

func (ith *ItemHandler) GetAllItems(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	items, err := ith.its.GetItemsList(user.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}

	itemResponse := []ItemListRes{}
	for _, i := range items {
		image := ItemImageRes{}

		item := ItemListRes{}
		item.ID = i.ID
		item.ItemName = i.ItemName
		item.Description = i.Description
		item.Serve = i.Serve
		item.Ingredient = i.Ingredient
		item.Method = i.Method
		item.UserID = i.UserID
		item.Image = image
		itemResponse = append(itemResponse, item)
	}

	c.JSON(200, itemResponse)
}

func (ith *ItemHandler) GetItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	item, err := ith.its.GetItemById(uint(itemID))
	if err != nil {
		Error(c, 500, err)
		return
	}

	imageRes := ItemImageRes{}
	image, err := ith.itemImages.GetImageByItemID(uint(itemID))
	if err == nil {
		imageRes.ID = image.ID
		imageRes.Filename = image.Filename
		imageRes.itemID = image.ItemID
	}

	itemRes := ItemResponse{
		ID:          item.ID,
		ItemName:    item.ItemName,
		Description: item.Description,
		Serve:       item.Serve,
		Ingredient:  item.Ingredient,
		Method:      item.Method,
		UserID:      item.UserID,
		Image:       imageRes,
	}

	c.JSON(200, itemRes)
}

func (ith *ItemHandler) RemoveItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	if err := ith.its.DeleteItem(uint(itemID), user.ID); err != nil {
		Error(c, 500, err)
		return
	}

	c.Status(200)
}

func (ith *ItemHandler) UpdateItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	itemUpdate := new(ItemUpdate)
	if err := c.BindJSON(itemUpdate); err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	item := new(models.Item)
	item.ItemName = itemUpdate.ItemName
	item.Description = itemUpdate.Description
	item.Serve = itemUpdate.Serve
	item.Ingredient = itemUpdate.Ingredient
	item.Method = itemUpdate.Method

	if err = ith.its.UpdateItem(item, uint(itemID)); err != nil {
		Error(c, 500, err)
		return
	}

	update, err := ith.its.GetItemById(uint(itemID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	res := ItemResponse{
		ID:          update.ID,
		ItemName:    update.ItemName,
		Description: update.Description,
		Serve:       update.Serve,
		Ingredient:  update.Ingredient,
		Method:      update.Method,
		UserID:      update.UserID,
	}

	c.JSON(200, res)

}
