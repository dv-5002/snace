import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text, TouchableWithoutFeedback, ScrollView, Image, TouchableOpacity, StatusBar } from 'react-native';
import { TextInput, IconButton } from 'react-native-paper';
import { Button, Icon, Input, StyleService, useStyleSheet, Spinner } from '@ui-kitten/components';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { Formik } from 'formik';
import { EmailIcon, EyeIcon, EyeOffIcon, PersonIcon, PlusIcon } from '../../components/Icons';
import { ImageOverlay } from '../../components/image-overlay'
import { useDispatch } from 'react-redux';
import { registerNewUser } from '../../api';

const RegisterForm = ({ onClickRegister }) => {
    const navigation = useNavigation()
    const styles = useStyleSheet(themedStyles);
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('')
    const [passwordVisible, setPasswordVisible] = useState(true);
    const [confirmPasswordVisible, setConfirmPasswordVisible] = useState(true)

    const checkFirstname = firstname && firstname.length > 4;
    const checkLastname = lastname && lastname.length > 4;
    const checkEmail = email && email.length > 4;
    const checkPassword = password && password.length > 4;
    const checkConfirmPassword = password == confirmPassword

    const [firstnameFocus, setFirstnameFocus] = useState(false);
    const [lastnameFocus, setLastnameFocus] = useState(false);
    const [emailFocus, setEmailFocus] = useState(false);
    const [passFocus, setPassFocus] = useState(false);
    const [confirmPassFocus, setConfirmPassFocus] = useState(false);

    const [isLoading, setIsLoading] = useState(false)
    const dispatch = useDispatch()
    const onPasswordIconPress = () => {
        setPasswordVisible(!passwordVisible);
    };

    const onConfirmPasswordIconPress = () => {
        setConfirmPasswordVisible(!confirmPasswordVisible);
    }

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('white-content');
        }, [])
    )

    const renderIconPassword = (props) => (
        <TouchableWithoutFeedback onPress={onPasswordIconPress}>
            <Icon {...props} name={passwordVisible ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const renderIconConfirmPass = (props) => (
        <TouchableWithoutFeedback onPress={onConfirmPasswordIconPress}>
            <Icon {...props} name={confirmPasswordVisible ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const handleSignup = () => {
        const formData = new FormData()
        formData.append("email", email)
        formData.append("firstname", firstname)
        formData.append("lastname", lastname)
        formData.append("password", password)
        console.log('login', formData)
        registerNewUser(formData)
            .then((response) => {
                console.log(response.data.token)
                dispatch(isLogin(response.data.token))
                navigation.navigate("Home", {
                    user_id: response.data.user_id
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1 }}>
            <ImageOverlay
                style={styles.container}
                source={require('../../assets/image-background.jpg')}>
                <View style={styles.headerContainer}>
                    <Image
                        style={{ width: 250, height: 216 }}
                        resizeMode="contain"
                        source={require('../../assets/logo.png')} />
                </View>

                <View style={styles.formContainer}>
                    <View style={styles.firstForm}>
                        <Input
                            style={styles.nameInput}
                            status={!firstnameFocus || checkFirstname ? 'control' : 'danger'}
                            caption={!firstnameFocus || checkFirstname ? '' : 'Firstname must contain at least 4 characters'}
                            autoCapitalize='none'
                            placeholder='Firstname'
                            value={firstname}
                            onChangeText={setFirstname}
                            onFocus={() => { setFirstnameFocus(true); }}
                        />
                        <Input
                            style={styles.lastnameInput}
                            status={!lastnameFocus || checkLastname ? 'control' : 'danger'}
                            caption={!lastnameFocus || checkLastname ? '' : 'Lastname must contain at least 4 characters'}
                            autoCapitalize='none'
                            placeholder='Lastname'
                            value={lastname}
                            onChangeText={setLastname}
                            onFocus={() => { setLastnameFocus(true); }}
                        />
                    </View>

                    <Input
                        style={styles.formInput}
                        status={!emailFocus || checkEmail ? 'control' : 'danger'}
                        caption={!emailFocus || checkEmail ? '' : 'Email must contain at least 4 characters'}
                        autoCapitalize='none'
                        placeholder='Email'
                        accessoryRight={EmailIcon}
                        value={email}
                        onChangeText={setEmail}
                        onFocus={() => { setEmailFocus(true); }}
                    />
                    <Input
                        style={styles.formInput}
                        status={!passFocus || checkPassword ? 'control' : 'danger'}
                        caption={!passFocus || checkPassword ? '' : 'Password must contain at least 4 characters'}
                        autoCapitalize='none'
                        secureTextEntry={passwordVisible}
                        placeholder='Password'
                        accessoryRight={renderIconPassword}
                        value={password}
                        onChangeText={setPassword}
                        onIconPress={onPasswordIconPress}
                        onFocus={() => { setPassFocus(true); }}
                    />
                    <Input
                        style={styles.formInput}
                        status={!confirmPassFocus || checkConfirmPassword ? 'control' : 'danger'}
                        caption={!confirmPassFocus || checkConfirmPassword ? '' : 'Password mismatch'}
                        autoCapitalize='none'
                        secureTextEntry={confirmPasswordVisible}
                        placeholder='Confirm password'
                        accessoryRight={renderIconConfirmPass}
                        value={confirmPassword}
                        onChangeText={setConfirmPassword}
                        onIconPress={onConfirmPasswordIconPress}
                        onFocus={() => { setConfirmPassFocus(true); }}
                    />
                </View>

                <View>
                    <Button
                        style={styles.signUpButton}
                        size='giant'
                        disabled={!checkFirstname || !checkLastname || !checkEmail || !checkPassword || !checkConfirmPassword}
                        onPress={() => handleSignup()}>
                        SIGN UP
                    </Button>
                    <View style={styles.signUpContainer}>
                        <Text style={styles.text}>
                            Already have an account?
                        </Text>
                        <TouchableOpacity
                            style={{ marginLeft: 5 }}
                            onPress={() => navigation.navigate('Signin')}>
                            <Text style={styles.signInButton}>
                                Sign In
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageOverlay>
        </ScrollView>

    )
}

const themedStyles = StyleService.create({
    container: {
        flex: 1,
    },
    headerContainer: {
        minHeight: 216,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer: {
        paddingHorizontal: 16,
    },
    formInput: {
        marginTop: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
    signUpButton: {
        marginTop: 32,
        marginHorizontal: 16,
        borderColor: 'transparent'
    },
    signInButton: {
        fontWeight: 'bold',
        color: '#00cc99'
    },
    text: {
        color: 'white'
    },
    signUpContainer: {
        marginVertical: 16,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    firstForm: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    nameInput: {
        width: 178,
        backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
    lastnameInput: {
        marginLeft: 5,
        width: 178,
        backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
});

export default RegisterForm;