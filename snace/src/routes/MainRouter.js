import React from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome5'
import RecipeList from '../screens/Recipe/RecipeList'
import Profile from '../screens/User/Profile'

const Tab = createMaterialBottomTabNavigator()
const MainRouter = () => {

  function getActiveRoute(navigationState) {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    if (route.routes) {
      return getActiveRoute(route);
    }
    return route;
  }

  return (
    <>
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#00cc99"
        inactiveColor="#A7A7A7"
        shifting={true}
        barStyle={{ backgroundColor: 'white' }}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName
            if (route.name === 'Home') {
              iconName = 'home'
              size = 20
            } else if (route.name === 'Profile') {
              iconName = 'user'
              size = 20
            }
            return <Icon name={iconName} size={size} color={color} />
          },
        })}>
        <Tab.Screen
          name="Home"
          component={RecipeList} />
        <Tab.Screen
          name="Profile"
          component={Profile} />
      </Tab.Navigator>
    </>
  )
}

export default MainRouter