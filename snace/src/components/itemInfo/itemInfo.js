import React, { useState } from 'react';
import { ScrollView, View, YellowBox, StatusBar } from 'react-native';
import { Button, Card, Icon, List, StyleService, Text, useStyleSheet } from '@ui-kitten/components';
import { backIcon, heartIcon, clickHeartIcon, shareIcon } from '../../components/Icons'
import { Appbar } from 'react-native-paper'
import { ImageOverlay } from '../image-overlay';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { host } from '../../api';

const ItemDescription = ({ itemInformation }) => {
    const styles = useStyleSheet(themedStyles);
    const [heartIconVisible, setHeartIconVisible] = useState(false);
    const navigation = useNavigation();

    const onHeartIconPress = () => {
        setHeartIconVisible(!heartIconVisible);
    }

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('white-content');
        }, [])
    )

    return (
        <>
            <View style={styles.appbarContainer}>
                <Appbar
                    style={styles.appbar}>
                    <Appbar.Content />
                    <Button
                        style={styles.button}
                        appearance='ghost'
                        status='danger'
                        accessoryRight={heartIconVisible ? clickHeartIcon : heartIcon}
                        onPress={onHeartIconPress} />
                    <Button
                        style={styles.button}
                        appearance='ghost'
                        status='control'
                        accessoryRight={shareIcon}
                        onPress={(() => navigation.goBack())} />
                </Appbar>
            </View>

            <ScrollView
                showsVerticalScrollIndicator={false}
                style={styles.container}>
                <ImageOverlay
                    style={styles.image}
                    source={
                        itemInformation.image === undefined || itemInformation.image.filename === "" ?
                            { uri: 'https://uxrecipe.github.io/img/uxrecipe-logo.png' }
                            :
                            { uri: host + "/image/" + itemInformation.id + "/" + itemInformation.image.filename }
                    }
                />
                <Card
                    style={styles.titleCard}
                    appearance='filled'
                    disabled={true}>
                    <Text
                        style={styles.title}
                        category='h1'>
                        {itemInformation.itemName}
                    </Text>
                </Card>
                <Text
                    style={styles.sectionLabel}
                    category='s1'>
                    Description
                 </Text>
                <Text
                    style={styles.description}
                    appearance='hint'>
                    {itemInformation.description}
                </Text>
                <Text
                    style={styles.sectionLabel}
                    category='s1'>
                    Ingredients
      </Text>
                <Text
                    style={styles.description}
                    appearance='hint'>
                    {itemInformation.ingredient}
                </Text>
                <Text
                    style={styles.sectionLabel}
                    category='s1'>
                    Methods
      </Text>
                <Text
                    style={styles.description}
                    appearance='hint'>
                    {itemInformation.method}
                </Text>
            </ScrollView>
        </>
    );
};

const themedStyles = StyleService.create({
    appbar: {
        backgroundColor: 'transparent',
        marginTop: 24,
    },
    description: {
        marginHorizontal: 16,
        marginVertical: 8,
    },
    sectionLabel: {
        marginHorizontal: 16,
        marginVertical: 8,
    },
    button: {
        marginLeft: -16
    },
    appbarContainer: {
        backgroundColor: 'transparent',
        marginBottom: -88,
        zIndex: 1,
    }
});

export default ItemDescription;