package models

import (
	"errors"
	"final-project-backend/hash"
	"final-project-backend/rand"
	"fmt"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const cost = 12

type User struct {
	gorm.Model
	Email     string `gorm:"unique_index;not null", json:"email"`
	Password  string `gorm:"not null", json:"password"`
	Firstname string `gorm:"not null", json:"firstname"`
	Lastname  string `gorm:"not null", json:"lastname"`
	Token     string `gorm:"unique_index"`
	Image     string `json:"image"`
}

type UserGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

type UserService interface {
	CreateUser(userReq *User) error
	FindEmailAvailability(email string) (bool, error)
	UserLogin(userReq *User) (string, error)
	FindUserByEmail(email string) (*User, error)
	GetUserByToken(token string) (*User, error)
	FindUserById(id uint) (*User, error)
	UpdateToken(userID uint) error
	Update(userReq *User) error
}

func NewUserService(db *gorm.DB, hmac *hash.HMAC) UserService {
	return &UserGorm{db, hmac}
}

func (ug *UserGorm) CreateUser(userReq *User) error {
	user := new(User)
	user.Email = userReq.Email

	hash, err := bcrypt.GenerateFromPassword([]byte(userReq.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	user.Firstname = userReq.Firstname
	user.Lastname = userReq.Lastname

	token, err := rand.GenerateToken()
	if err != nil {
		return err
	}
	hashTokenStr := ug.hmac.Hash(token)
	user.Token = hashTokenStr
	return ug.db.Create(user).Error
}

func (ug *UserGorm) FindEmailAvailability(email string) (bool, error) {
	user := User{}
	if err := ug.db.Where("email = ?", email).First(&user).Error; err != nil {
		return true, nil
	}

	return false, nil
}

func (ug *UserGorm) FindUserByEmail(email string) (*User, error) {
	user := User{}
	if err := ug.db.Where("email = ?", email).First(&user).Error; err != nil {
		return nil, err
	}

	return &user, nil
}

func (ug *UserGorm) UserLogin(userReq *User) (string, error) {
	userFound, err := ug.FindUserByEmail(userReq.Email)
	if err != nil {
		return "", errors.New("Email is not found")
	}

	hashPassword := userFound.Password
	err = bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(userReq.Password))
	if err != nil {
		return "", errors.New("Password is not correct")
	}

	newToken, err := rand.GenerateToken()
	if err != nil {
		return "", errors.New("Cannot generate token")
	}
	hashTokenStr := ug.hmac.Hash(newToken)

	err = ug.db.Model(&User{}).Where("id = ?", userFound.ID).Update("token", hashTokenStr).Error
	if err != nil {
		return "", errors.New("User Not found")
	}
	return newToken, nil
}

func (ug *UserGorm) GetUserByToken(token string) (*User, error) {
	hashToken := ug.hmac.Hash(token)
	fmt.Println("generate new token ===> ", hashToken)

	user := User{}
	err := ug.db.Where("token = ?", hashToken).First(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (ug *UserGorm) FindUserById(id uint) (*User, error) {
	user := User{}
	err := ug.db.Where("id = ?", id).First(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (ug *UserGorm) UpdateToken(userID uint) error {
	userFound, err := ug.FindUserById(userID)
	if err != nil {
		return err
	}

	token, err := rand.GenerateToken()
	if err != nil {
		return err
	}

	hashToken := ug.hmac.Hash(token)
	err = ug.db.Model(&userFound).Where("id = ?", userID).Update("token", hashToken).Error
	if err != nil {
		return err
	}
	return nil
}

func (ug *UserGorm) Update(userReq *User) error {
	user := User{}
	if err := ug.db.Model(&user).Save(&userReq).Error; err != nil {
		return err
	}
	return nil
}
