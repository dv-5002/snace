import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import MainRouter from './MainRouter'
import Signup from '../screens/User/Signup'
import Signin from '../screens/User/Signin'
import RecipeDetail from '../screens/Recipe/RecipeDetail'
import Create from '../screens/Recipe/Create'
import Profile from '../screens/User/Profile'
import EditProfile from '../screens/User/EditProfile'

const Stack = createStackNavigator()

export const StackRouter = ({ token }) => (

  <NavigationContainer>
    {token ?
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name='Home' component={MainRouter} />
      </Stack.Navigator>
      :
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name='Signin' component={Signin} />
        <Stack.Screen name='Signup' component={Signup} />
        <Stack.Screen name='Home' component={MainRouter} />
        <Stack.Screen name='RecipeDetail' component={RecipeDetail} />
        <Stack.Screen name='Create' component={Create} />
        <Stack.Screen name='Profile' component={Profile} />
        <Stack.Screen name='EditProfile' component={EditProfile} />
      </Stack.Navigator>
    }
  </NavigationContainer>
)