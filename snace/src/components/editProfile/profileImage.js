import React from 'react';
import { View, StyleSheet, TouchableHighlight, Alert, StatusBar } from 'react-native';
import { host, uploadUserImage } from '../../api';
import { requestCameraPermission, requestWriteStoragePermission } from '../uploadImagePermission';
import ImagePicker from 'react-native-image-picker';
import { Avatar, Button } from '@ui-kitten/components';
import { ProfileSetting } from '../../components/ProfileSetting';
import { useFocusEffect } from '@react-navigation/native';
import { cameraIcon } from '../../components/Icons';

const ProfileImage = ({ userImage, setUserImage, userInfo, token }) => {

    const upload = () => {
        ImagePicker.showImagePicker({}, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                setUserImage(image);
                uploadUserImage(image, token)
                    .then((res) => {
                        setTimeout(() => {
                            Alert.alert("Success", 'Profile image uploaded');
                        }, 1000);
                    })
                    .catch((err) => {
                        setTimeout(() => {
                            Alert.alert("Error", 'Upload profile image is not success, please try again');
                        }, 1000);
                    })
            }
        });
    }

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('dark-content');
        }, [])
    )

    const uploadProfileImage = () => {
        requestCameraPermission(() => requestWriteStoragePermission(upload));
    }

    return (
        <View style={styles.profileImgSection}>
            <View style={styles.imgBlock}>
                <Avatar
                    source={
                        userInfo.image.filename === "" && userImage === "" ?
                            {uri:'https://seeklogo.com/images/C/chef-logo-1B1BA8D5BF-seeklogo.com.png'}
                            :
                            userImage === "" ?
                                { uri: host + "/userImage/" + userInfo.id + "/" + userInfo.image.filename }
                                :
                                { uri: userImage.uri }
                    }
                    style={{width:200,height:200}}
                />
            </View>
            <View style={styles.cameraIconBlock}>
                <Button
                    accessoryLeft={cameraIcon}
                    onPress={() => uploadProfileImage()}
                    style={styles.touchHighlight}
                >
                </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgBlock: {
        flex: 0,
        margin: 35
    },
    cameraIconBlock: {
        position: 'absolute',
        right: 110,
        top: 40
    },
    touchHighlight: {
        borderRadius: 100
    },
})

export default ProfileImage;