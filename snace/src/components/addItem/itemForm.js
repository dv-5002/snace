import React, { useState, useRef } from 'react';
import { View, StyleSheet, ImageBackground } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { requestCameraPermission, requestWriteStoragePermission } from '../uploadImagePermission';
import { Formik } from 'formik';
import { ImageOverlay } from '../../components/image-overlay';
import { Text, Button, Spinner, Input } from '@ui-kitten/components';

const ItemForm = ({ itemImage, setItemImage, onClickSubmit }) => {
    const itemnameInput = useRef();
    const servesInput = useRef();
    const itemDescriptInput = useRef();
    const ingredientsInput = useRef();
    const methodsInput = useRef();

    const upload = () => {
        ImagePicker.showImagePicker({}, (image) => {
            console.log('image: ', image)
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                setItemImage(image);
            }
        });
    }

    const uploadProfileImage = () => {
        requestCameraPermission(() => requestWriteStoragePermission(upload));
    }

    return (
        <Formik
            initialValues={{
                itemName: "",
                ingredient: "",
                serve: "",
                description: "",
                method: ""
            }}
            enableReinitialize={false}
            onSubmit={(values, actions) => {
                onClickSubmit(values);
                setTimeout(() => {
                    actions.setSubmitting(false);
                    actions.resetForm({ values: '' })
                }, 1000);

            }}
        >
            {({ handleChange, handleSubmit, values, isSubmitting }) => (
                <View style={styles.itemFormSection}>
                    <View style={styles.uploadImage} >
                        {itemImage !== "" ?
                            <View style={styles.imageBlock}>
                                <ImageOverlay source={{ uri: itemImage.uri }} style={styles.image}>
                                    <Button
                                        onPress={() => uploadProfileImage()}>
                                        Upload Cover
                                        </Button>
                                </ImageOverlay>
                            </View>
                            :
                            <View style={styles.uploadBlock}>
                                <Button
                                    onPress={() => uploadProfileImage()}>
                                    Upload Cover
                                </Button>
                            </View>
                        }
                    </View>

                    <View style={styles.textInputSection}>
                        <View style={styles.textInputBlock}>
                            <View style={styles.textInput}>
                                <Input
                                    name="itemName"
                                    mode='outlined'
                                    placeholder='Recipe name'
                                    value={values.itemName}
                                    style={styles.input}
                                    onChangeText={handleChange('itemName')}
                                    ref={itemnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                />
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.textInput}>
                                <Input
                                    name="description"
                                    mode='outlined'
                                    multiline
                                    placeholder='Description'
                                    value={values.description}
                                    onChangeText={handleChange('description')}
                                    maxLength={150}
                                    numberOfLines={3}
                                    ref={itemDescriptInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                />
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.textInput}>
                                <Input
                                    name="ingredient"
                                    mode='outlined'
                                    multiline
                                    placeholder='Ingredient'
                                    value={values.ingredient}
                                    onChangeText={handleChange('ingredient')}
                                    maxLength={150}
                                    numberOfLines={3}
                                    ref={ingredientsInput}
                                />
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.textInput}>
                                <Input
                                    name="serve"
                                    mode='outlined'
                                    placeholder='Serve'
                                    value={values.serve}
                                    style={styles.input}
                                    onChangeText={handleChange('serve')}
                                    ref={servesInput}
                                    keyboardType="number-pad"
                                />
                            </View>
                        </View>

                        <View style={styles.textInputBlock}>
                            <View style={styles.textInput}>
                                <Input
                                    name="method"
                                    mode='outlined'
                                    multiline
                                    placeholder='Method'
                                    value={values.method}
                                    onChangeText={handleChange('method')}
                                    maxLength={150}
                                    numberOfLines={3}
                                    ref={methodsInput}
                                />
                            </View>
                        </View>

                        <View style={styles.buttonBlock}>
                            {isSubmitting ? (
                                <Spinner size="small" color="#3FC6DD" style={{ alignItems: 'center' }}></Spinner>
                            ) : (
                                    <Button
                                        mode="contained"
                                        onPress={handleSubmit}>
                                        Create Recipe
                                    </Button>
                                )}
                        </View>
                    </View>
                </View>
            )}
        </Formik>
    )
}

const styles = StyleSheet.create({
    itemFormSection: {
        flex: 1,
    },
    uploadImage: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    uploadBlock: {
        flex: 0,
        width: '100%',
        height: 250,
        backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageBlock: {
        flex: 0,
        width: '100%',
        height: 250,
        backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: '100%',
        height: 250,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        marginVertical: 16,
        margin: 16
    },
    buttonBlock:{
        margin:16,
        alignItems:'center'
    }
})
export default ItemForm;