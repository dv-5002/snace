import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert, StatusBar } from 'react-native';
import ItemForm from '../../components/addItem/itemForm';
import { addNewItem, uploadImage } from '../../api';
import { connect } from 'react-redux';
import { useNavigation, useFocusEffect } from '@react-navigation/native';

const Create = ({ token }) => {
    const [itemInfo, setItemInfo] = useState({});
    const [image, setImage] = useState("");
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    const addedSuccess = () => {
        setTimeout(() => {
            navigation.navigate('Home');
            setIsLoading(false);
            setImage("");
        }, 500);
    }

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('white-content');
        }, [])
    )

    const submitForm = (formvalue) => {
        addNewItem(formvalue, itemInfo, token)
            .then((res) => {
                const id = res.data.id
                if (image === "") {
                    addedSuccess();
                } else {
                    uploadImage(id, image, token)
                        .then(() => {
                            addedSuccess();
                        })
                        .catch((err) => {
                            console.log(err);
                            navigation.navigate('Home');
                            Alert.alert("Error", "Add item image is not success")
                        })
                }
            })
            .catch((err) => {
                setIsLoading(false);
                console.log(err);
                Alert.alert("Error", "Add item is not success, please try again")
            })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setImage("");
        });
        return unsubscribe;

    }, [navigation]);

    return (
        <View style={styles.container}>
            <ItemForm
                itemInfo={itemInfo}
                setItemInfo={setItemInfo}
                itemImage={image}
                setItemImage={setImage}
                onClickSubmit={submitForm}
                isLoading={isLoading} />
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(Create);