import React from 'react'
import { StyleSheet } from 'react-native';
import { FAB } from 'react-native-paper'
import { useNavigation } from '@react-navigation/native'

const AddFab = () => {
  const navigation = useNavigation()

  return (
    <FAB
      style={styles.fab}
      icon="plus"
      onPress={() => navigation.navigate('Create')}
    />
  )
}

const styles = StyleSheet.create({
  fab: {
      position: 'absolute',
      margin: 15,
      right: 0,
      bottom: 0,
      backgroundColor:'white'
    }
})

export default AddFab