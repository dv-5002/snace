import React, { useState, useEffect } from 'react';
import { View, StyleSheet, SafeAreaView, FlatList, TouchableHighlight, Image, ImageBackground } from 'react-native';
import { IconButton, Button, ActivityIndicator } from 'react-native-paper';
import NoData from '../noData';
import { useNavigation } from '@react-navigation/native';
import { host, removeItem } from '../../api';
import { Card, Text, Avatar } from '@ui-kitten/components';
import LinearGradient from 'react-native-linear-gradient'
import { getUserProfile } from '../../api';
import { setUserInfo } from '../../actions/userAction';

const ItemList = ({ title, itemsData, searchValue, userInfo, token }) => {
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    const checkEmptyData = (data) => {
        if (data.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (token) {
                getUserProfile(token)
                    .then((res) => {
                        setUserInfo(res.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            }
        });
        return unsubscribe;
    }, [token, navigation]);

    const checkFilter = (items, searchValue) => {
        if (searchValue === '') {
            return items;
        } else {
            return items.filter((item) => item.itemName.toLowerCase().match(searchValue.toLowerCase()));
        }
    }

    const renderItemHeader = (item) => (

        < ImageBackground
            style={styles.itemHeader}
            source={
                item.image === undefined || item.image.filename === "" ?
                    { uri: 'https://uxrecipe.github.io/img/uxrecipe-logo.png' }
                    :
                    { uri: host + "/image/" + item.id + "/" + item.image.filename }
            }
        >
            <LinearGradient
                style={styles.linearGradient}
                colors={['#000', 'transparent']} start={{ x: 0.5, y: 0.2 }} end={{ x: 0.5, y: 1 }}>
                <Text
                    style={styles.itemTitle}
                    category='h3'
                    status='control'>
                    {item.itemName}
                </Text>
            </LinearGradient>
        </ImageBackground >
    )

    const Item = (item) => {
        return (
            <View style={styles.mainBlock} >
                <Card
                    header={() => renderItemHeader(item)}
                    onPress={() => navigation.navigate('RecipeDetail', { itemId: item.id })}>
                    <View style={styles.itemFooter}>
                    </View>
                </Card>
            </View>
        )
    }

    return (
        <View style={styles.listSection}>
            {checkEmptyData(itemsData) ?
                <NoData screen={title} />
                :
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={checkFilter(itemsData, searchValue)}
                        renderItem={({ item }) => Item(item)}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={() => <NoData />}
                    />
                </SafeAreaView>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    listSection: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        padding: 5
    },
    container: {
        flex: 1
    },
    mainBlock: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5
    },
})


export default ItemList;