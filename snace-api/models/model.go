package models

import "github.com/jinzhu/gorm"

func AutoMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&Item{},
		&User{},
		&ItemImage{},
		&UserImage{},
	).Error
}
