export const SET_ALL_ITEMS_DATA = 'SET_ALL_ITEMS_DATA';
export const SET_ITEM = 'SET_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';

export const setAllItemsData = (items) => {
    return{
        type: SET_ALL_ITEMS_DATA,
        items
    }
}

export const setItem = ( itemId, userId ) => {
    return{
        type: SET_ITEM,
        itemId, userId
    }
}

export const removeItem = (itemId, userId) => {
    return {
        type: REMOVE_ITEM,
        itemId, userId
    }
}

export const editItemInformation = (itemInfo) => {
    return {
        type: EDIT_ITEM,
        itemInfo
    }
}