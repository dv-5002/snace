import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import ItemInfo from '../../components/itemInfo/itemInfo';
import { useRoute, useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import { getItemInfo } from '../../api';
import Loading from '../../components/Loading';

const RecipeDetail = ({ token }) => {
    const route = useRoute();
    const [itemId] = useState(route.params.itemId);
    const [itemInfo, setItemInfo] = useState({});
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        const unsubscribe = navigation.addListener('focus', () => {
            getItemInfo(itemId, token)
                .then((res) => {

                    setItemInfo(res.data);
                    setIsLoading(false);

                })
                .catch((err) => {
                    console.log(err);
                    setIsLoading(false);
                })
        });

        return unsubscribe;

    }, [navigation, itemId])

    return (
        <View style={styles.container}>
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <ItemInfo itemInformation={itemInfo} />
                </ScrollView>
            }


        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flexGrow: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

export default connect(mapStateToProps, null)(RecipeDetail);