import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Formik } from 'formik';
import { Spinner, Button,Input } from '@ui-kitten/components';

const ProfileForm = ({ profileInfo, updateProfile}) => {
    const [visible, setVisible] = useState(false);
    const firstnameInput = useRef();
    const lastnameInput = useRef();

    return (
        <Formik
            initialValues={{
                firstname: profileInfo.firstname,
                lastname: profileInfo.lastname,
            }}
            enableReinitialize={false}
            onSubmit={(values, actions) => {
                updateProfile(values);
                setTimeout(() => {
                    actions.setSubmitting(false);
                }, 1000);
            }}
        >
            {({ handleChange, handleSubmit, handleBlur, touched, isSubmitting }) => (
                <View style={styles.profileInfoSection}>
                    <View style={styles.form}>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>First Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <Input
                                    name="firstname"
                                    mode='outlined'
                                    value={values.firstname}
                                    style={styles.input}
                                    onChangeText={handleChange('firstname')}
                                    ref={firstnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.firstname && touched.firstname ? (
                                    <Text style={styles.errorText}>{errors.firstname}</Text>
                                ) : null}
                            </View>
                        </View>
                        <View style={styles.textInputBlock}>
                            <View style={styles.topic}>
                                <Text style={styles.topicText}>Last Name</Text>
                            </View>
                            <View style={styles.textInput}>
                                <Input
                                    name="lastname"
                                    mode='outlined'
                                    value={profileInfo.lastname}
                                    value={values.lastname}
                                    style={styles.input}
                                    onChangeText={handleChange('lastname')}
                                    ref={lastnameInput}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                />
                            </View>
                            <View style={styles.validateError}>
                                {errors.lastname && touched.lastname ? (
                                    <Text style={styles.errorText}>{errors.lastname}</Text>
                                ) : null}
                            </View>
                        </View>

                        <View style={styles.buttonBlock}>
                            {isSubmitting ? (
                                <Spinner size="small" color="#3FC6DD"/>
                            ) : (
                                    <Button
                                        mode="contained"
                                        uppercase={false}
                                        onPress={handleSubmit}>
                                        Update
                                    </Button>
                                )}

                        </View>
                    </View>
                </View>
            )
            }
        </Formik >
    )
}

const styles = StyleSheet.create({
    profileInfoSection: {
        flex: 1,
        backgroundColor: 'gray'
    },
    form: {
        flex: 0,
        margin: 15
    },
    textInputBlock: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 5,
        marginBottom: 15
    },
    topic: {
        marginTop: 5
    },
    topicText: {
        fontSize: 16,
        color: '#323232'
    },
    input: {
        height: 48,
        backgroundColor: '#F9F9F9'
    },
    buttonBlock: {
        flex: 0,
        marginTop: 15,
        marginBottom: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

export default ProfileForm;