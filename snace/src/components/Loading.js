import React from 'react';
import Spiner from 'react-native-spinkit'
import { View, StyleSheet } from 'react-native';

const Loading = ({ visible }) => {
    return (
        <View style={styles.container}>
            <Spiner isVisible={visible} size={90} type="Circle" color="#38E0A6" />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Loading;