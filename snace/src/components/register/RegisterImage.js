import React from 'react';
import { View, StyleSheet, TouchableHighlight } from 'react-native';
import { Avatar } from '@ui-kitten/components';

const RegisterImage = ({ profileImg }) => {
    return (
        <View style={styles.profileImgSection}>
            <View style={styles.imgBlock}>
                <Avatar
                    size={150}
                    source={
                        profileImg === '' ?
                            { uri: 'https://seeklogo.com/images/C/chef-logo-1B1BA8D5BF-seeklogo.com.png' }
                            :
                            { uri: profileImg }
                    } />
            </View>
            <View style={styles.cameraIconBlock}>
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor="#4B80D4"
                    onPress={() => alert('upload')}
                    style={styles.touchHighlight} >
                </TouchableHighlight>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    profileImgSection: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'gray'
    },
    imgBlock: {
        flex: 0,
        margin: 35
    },
    cameraIconBlock: {
        position: 'absolute',
        right: 122,
        top: 40
    },
    touchHighlight: {
        borderRadius: 100
    }
})

export default RegisterImage;