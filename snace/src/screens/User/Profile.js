import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import ProfileInfo from '../../components/profile/profileInfo';
import { connect } from 'react-redux';
import { setUserInfo } from '../../actions/userAction';
import { getUserProfile } from '../../api';
import { useNavigation } from '@react-navigation/native';
import AddFab from '../../components/AddFab';

const Profile = ({ userInfo, setUserInfo, token,itemsData }) => {
    const navigation = useNavigation();
    
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (token) {
                getUserProfile(token)
                    .then((res) => {
                        setUserInfo(res.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            }
        });
        return unsubscribe;
    }, [token, navigation]);

    return (
        <View style={styles.container}>
            <ProfileInfo userInformation={userInfo} />
            {console.log(itemsData)}
            <AddFab />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})

const mapStateToProps = (state) => {
    return {
        userInfo: state.user.userInfo,
        token: state.user.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUserInfo: (userInfo) => dispatch(setUserInfo(userInfo))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile);