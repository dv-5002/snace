import { SET_ALL_ITEMS_DATA, REMOVE_ITEM, EDIT_ITEM } from "../actions/itemsActions";

const initialItems = {
    itemsList: []
};

export const itemsReducer = (state = initialItems, action) => {
    switch (action.type) {
        case SET_ALL_ITEMS_DATA:
            return {
                itemsList: action.items
            }
        case REMOVE_ITEM:
            return {
                itemsList:
                    state.itemsList.map((item) => {
                        if (item.id === action.itemId && item.userId === action.userId) {
                            return {
                                ...item,
                                remove: true
                            }
                        } else {
                            return item
                        }
                    })
            }
        case EDIT_ITEM:
            return {
                itemsList:
                    state.itemsList.map((item) => {
                        if (item.id === action.itemInfo.id && item.userId === action.itemInfo.userId) {
                            return {
                                ...item,
                                itemName: action.itemInfo.itemName,
                                description: action.itemInfo.description,
                                serve: action.itemInfo.serve,
                                ingredient: action.itemInfo.ingredient,
                                method: action.itemInfo.method,
                            }
                        } else {
                            return item
                        }
                    })
            }
        default:
            return state;
    }
}