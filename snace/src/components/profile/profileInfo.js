import React, { useState } from 'react';
import { View, StyleSheet, ScrollView, StatusBar, Alert } from 'react-native';
import { useStyleSheet, StyleService, Button, Popover, Layout, Avatar, Text } from '@ui-kitten/components';
import { ImageOverlay } from '../image-overlay';
import { editIcon, moreIcon } from '../../components/Icons'
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { logout,host } from '../../api'

const ProfileInfo = ({ userInformation, setToken, token }) => {
    const navigation = useNavigation()
    const styles = useStyleSheet(themedStyle)
    const [visible, setVisible] = useState(false)

    const renderToggleButton = () => (
        <Button
            appearance='ghost'
            status='control'
            accessoryRight={moreIcon}
            onPress={() => setVisible(true)}>
        </Button>
    );

    const handleLogout = () => {
        logout(token)
            .then(() => {
                setTimeout(() => {
                    setToken("");
                    navigation.navigate('Signin')
                }, 1000);
            })
            .catch((err) => {
                setTimeout(() => {
                    console.log(err);
                    setTimeout(() => {
                        Alert.alert("Error", "Logout is not success, please try again");
                    }, 500);
                }, 1500);
            })
    }

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('white-content');
        }, [])
    )

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.container}>
            <ImageOverlay
                style={styles.header}
                source={require('../../assets/image-background.jpg')}>
                <View style={styles.iconButton}>
                    <Button
                        appearance='ghost'
                        status='control'
                        accessoryRight={editIcon}
                        onPress={() => {
                            navigation.navigate('EditProfile')
                        }}>
                    </Button>
                    <Popover
                        visible={visible}
                        anchor={renderToggleButton}
                        onBackdropPress={() => setVisible(false)}>
                        <Layout style={styles.content}>
                            <Button appearance='ghost'
                                onPress={() => handleLogout()}>
                                Logout
                            </Button>
                        </Layout>
                    </Popover>
                </View>
                <View style={styles.profileSection}>
                    <Avatar
                        style={{ height: 200, width: 200 }}
                        source={
                            userInformation.image.filename === "" ?
                                { uri: 'https://seeklogo.com/images/C/chef-logo-1B1BA8D5BF-seeklogo.com.png' }
                                :
                                { uri: host + "/userImage/" + userInformation.id + "/" + userInformation.image.filename }
                        }
                    />
                    <Text
                        style={styles.profileName}
                        category='h2'
                        status='control'>
                        {userInformation.firstname + " " + userInformation.lastname}
                    </Text>

                    <Text
                        style={styles.profileSocial}
                        status='control'>
                    </Text>
                </View>
            </ImageOverlay>
        </ScrollView>
    )
}

const themedStyle = StyleService.create({
    container: {
        flex: 1,
        backgroundColor: 'background-basic-color-2',
    },
    header: {
        paddingVertical: 24,
    },
    profileName: {
        zIndex: 1,
        color: 'white'
    },
    iconButton: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: 8
    },
    profileSection: {
        alignItems: 'center',
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
});

export default ProfileInfo;