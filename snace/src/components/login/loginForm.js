import React, { useState, useRef } from 'react';
import { View, StyleSheet, TouchableOpacity, Alert, Image, StatusBar, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon, Input, Text, Divider, Spinner } from '@ui-kitten/components';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { login } from '../../api';
import { setToken } from '../../actions/userAction';
import { connect } from 'react-redux';
import { ImageOverlay } from '../image-overlay';
import { PersonIcon } from '../Icons';

const LoginForm = ({ setToken }) => {
    const navigation = useNavigation();
    const emailInput = useRef();
    const passwordInput = useRef();
    const [loginForm, setLoginForm] = useState({
        email: "",
        password: ""
    });
    const [isLoading, setIsLoading] = useState(false);

    const [passwordVisible, setPasswordVisible] = useState(true);
    const [emailFocus, setEmailFocus] = useState(false);
    const [passFocus, setPassFocus] = useState(false);
    const checkEmail = loginForm.email && loginForm.email.length > 0;
    const checkPassword = loginForm.password && loginForm.password.length > 0;

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('white-content');
        }, [])
    )

    const onPasswordIconPress = () => {
        setPasswordVisible(!passwordVisible);
    };

    const renderIconPassword = (props) => (
        <TouchableWithoutFeedback onPress={onPasswordIconPress}>
            <Icon {...props} name={passwordVisible ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const onClickLogin = (form) => {
        setIsLoading(true);
        login(form)
            .then((res) => {
                setTimeout(() => {
                    setIsLoading(false);
                    setToken(res.data.token);
                    navigation.navigate('Home')
                    setLoginForm({
                        email: "",
                        password: ""
                    });
                }, 1000);
            })
            .catch((err) => {
                console.log(JSON.stringify(err));
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", 'Email or Password is not correct');
                }, 1000);

            })
    }

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1 }}>
            <ImageOverlay
                style={styles.container}
                source={require('../../assets/image-background.jpg')}>
                <View style={styles.headerContainer}>
                    <Image
                        style={{ width: 250, height: 250 }}
                        resizeMode="contain"
                        source={require('../../assets/logo.png')} />
                </View>
                <View style={styles.formContainer}>
                    <Input
                        ref={emailInput}
                        style={styles.input}
                        status={!emailFocus || checkEmail ? 'control' : 'danger'}
                        caption={!emailFocus || checkEmail ? '' : 'Can not be empty'}
                        placeholder='Email'
                        accessoryRight={PersonIcon}
                        value={loginForm.email}
                        onChangeText={(text) => setLoginForm({
                            ...loginForm,
                            email: text
                        })}
                        onFocus={() => { setEmailFocus(true); }}
                    />
                    <Input
                        ref={passwordInput}
                        style={styles.input}
                        status={!passFocus || checkPassword ? 'control' : 'danger'}
                        caption={!passFocus || checkPassword ? '' : 'Can not be empty'}
                        placeholder='Password'
                        accessoryRight={renderIconPassword}
                        value={loginForm.password}
                        secureTextEntry={passwordVisible}
                        onChangeText={(text) => setLoginForm({
                            ...loginForm,
                            password: text
                        })}
                        onIconPress={onPasswordIconPress}
                        onFocus={() => { setPassFocus(true); }}
                    />
                </View>

                <View style={styles.signInForm}>
                    {isLoading ?
                        <Spinner size="small" color="#3FC6DD" />
                        :
                        <Button
                            style={styles.signInButton}
                            size='giant'
                            disabled={!checkEmail || !checkPassword}
                            onPress={() => onClickLogin(loginForm)}>
                            SIGN IN
                    </Button>
                    }

                    <View style={styles.socialAuthContainer}>
                        <Divider style={styles.divider} />
                        <Text
                            style={styles.socialAuthHintText}
                            status='control'>
                            Or
                    </Text>
                        <Divider style={styles.divider} />
                    </View>

                    <View style={styles.signUpContainer}>
                        <Text style={styles.text}>
                            Don't have an account?
                        </Text>
                        <TouchableOpacity
                            style={{ marginLeft: 5 }}
                            onPress={() => navigation.navigate('Signup')}>
                            <Text style={styles.signUpButton}>
                                Sign Up
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageOverlay>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainer: {
        minHeight: 224,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer: {
        paddingHorizontal: 16,
    },
    signInForm: {
        paddingHorizontal: 16,
        marginTop: 64
    },
    input: {
        marginTop: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
    signInButton: {
        borderColor: 'transparent',
    },
    signUpButton: {
        fontWeight: 'bold',
        color: '#00cc99'
    },
    socialAuthContainer: {
        marginTop: 32,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    socialAuthHintText: {
        alignSelf: 'center',
        marginTop: -10,
    },
    divider: {
        width: 150
    },
    text: {
        color: 'white'
    },
    signUpContainer: {
        marginVertical: 16,
        flexDirection: 'row',
        justifyContent: 'center'
    },
});

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => dispatch(setToken(token))
    }
}
export default connect(null, mapDispatchToProps)(LoginForm);