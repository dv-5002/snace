import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Searchbar, Appbar } from 'react-native-paper';

const SearchBar = ({ searchValue, setSearchValue }) => {
    const onChangeSearch = (text) => {
        setSearchValue(text);
    }

    return (
            <Appbar style={styles.appbar}>
                <Image
                    style={{ width: 100, height: 100, marginLeft: 8 }}
                    resizeMode="contain"
                    source={require('../../assets/logo.png')} />
                <View style={styles.searchContainer}>
                    <Searchbar
                        placeholder="Search item"
                        onChangeText={text => onChangeSearch(text)}
                        value={searchValue}
                        style={styles.searchBar}
                        inputStyle={styles.input}
                    />
                </View>
            </Appbar>
    )
}

const styles = StyleSheet.create({
    searchBar: {
        margin: 10,
        height: 40,
        width: '80%',
        justifyContent: 'center'
    },
    input: {
        fontSize: 12,
        fontFamily: 'Prompt-Medium'
    },
    appbar: {
      paddingTop: 56,
      paddingBottom: 32,
      backgroundColor: 'white'
    },
    searchContainer: {
      flex: 1,
      alignItems: 'flex-end'
    }
})

export default SearchBar;