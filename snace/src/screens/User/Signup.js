import React, { useState } from 'react';
import { View, StyleSheet, ScrollView, Alert } from 'react-native';
import RegisterForm from '../../components/register/registerForm';
import { registerNewUser, checkEmailAvailability } from '../../api/index';
import { useNavigation } from '@react-navigation/native';
import { login } from '../../api';

const Signup = () => {
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);
    
    const register = (formValue) => {
        console.log(formValue);
        checkEmailAvailability(formValue.email)
            .then((res) => {
                if (res.data.result) {
                    registerNewUser(formValue)
                        .then(() => {
                            setTimeout(() => {
                                setIsLoading(false);
                                cleanTextInput();
                                Alert.alert("Success", "Registration success");
                                navigation.navigate('Login');
                            }, 1000);
                        })
                        .catch((err) => {
                            console.log(err);
                            setTimeout(() => {
                                setIsLoading(false);
                                cleanTextInput();
                                Alert.alert("Error", "Registration is not success, please try again");
                                navigation.navigate('MainScreen');
                            }, 1000);
                        })
                } else {
                    setTimeout(() => {
                    Alert.alert("Warning", "Email " + formValue.email  + " is already used");
                    }, 1000);
                }
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", "Cannot connect to the internet, please try again");
                    cleanTextInput();
                }, 1000);
            })
    }

    return (
        <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <RegisterForm
                    onClickRegister={register}
                    isLoading={isLoading}
                    setIsLoading={setIsLoading} />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flexGrow: 1
    },
})

export default Signup;