import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Caption } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const Nodata = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.noDataSection}>
            <View style={styles.imgSection}>
                <Image
                    source={require('../assets/empty-box.png')}
                    style={styles.img}
                />
            </View>
            <View style={styles.textSection}>
                <Caption style={styles.text}>Empty</Caption>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    noDataSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgSection: {
        flex: 0,
        margin: 5
    },
    img: {
        width: 110,
        height: 110
    },
    textSection: {
        flex: 0,
        marginTop: 10,
        marginBottom: 10
    },
    text: {
        fontFamily: 'JosefinSans-Medium',
        fontSize: 15
    },
})

export default Nodata;