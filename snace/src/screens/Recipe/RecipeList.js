import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert, StatusBar } from 'react-native';
import SearchBar from '../../components/home/searchBar';
import ItemList from '../../components/home/itemList';
import { connect, useDispatch } from 'react-redux';
import { getItemsList, getUserProfile } from '../../api';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import Loading from '../../components/Loading';
import { setUserInfo } from '../../actions/userAction';
import AddFab from '../../components/AddFab';

const RecipeList = ({ token, setUserInfo }) => {
    const dispatch = useDispatch()
    const [searchValue, setSearchValue] = useState('');
    const [itemsList, setItemsList] = useState([]);
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('dark-content');
        }, [])
    )

    const fetchItemsList = () => {
        getItemsList(token)
            .then((res) => {
                setItemsList(res.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 2000);
            })
            .catch((err) => {
                console.log(err);
                setTimeout(() => {
                    setIsLoading(false);
                    Alert.alert("Error", "Cannot connect internet, please try again")
                }, 1000);
            })
    }

    useEffect(() => {
        if (token) {
            getUserProfile(token)
                .then((res) => {
                    dispatch(setUserInfo(res.data))
                    })
                .catch((err) => {
                    console.log(err);
                })
        }
    }, [token]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (token) {
                setIsLoading(true);
                fetchItemsList();
            }
        });
        return unsubscribe;

    }, [token, navigation]);

    return (
        <View style={styles.container}>
            <SearchBar
                searchValue={searchValue}
                setSearchValue={setSearchValue}
            />
            {isLoading ?
                <Loading visible={isLoading} />
                :
                <ItemList
                    itemsData={itemsList}
                    searchValue={searchValue}
                    token={token}
                    refreshData={fetchItemsList}
                />
            }
            <AddFab />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        token: state.user.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUserInfo: (userInfo) => dispatch(setUserInfo(userInfo))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipeList);