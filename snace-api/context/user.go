package context

import (
	"final-project-backend/models"

	"github.com/gin-gonic/gin"
)

const key string = "user"

func SetUser(c *gin.Context, user *models.User) {
	c.Set(key, user)
}

func GetUser(c *gin.Context) *models.User {
	user, ok := c.Value("user").(*models.User)
	if !ok {
		return nil
	}
	return user
}
