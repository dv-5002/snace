import React from 'react';
import { StyleSheet } from 'react-native';
import { Divider, Layout, Input, Text } from '@ui-kitten/components';

export const ProfileSetting = (props) => {

  const { style, hint, value, ...layoutProps } = props;

  const renderHintElement = () => (
    <Text
      appearance='hint'
      category='s1'>
      {hint}
    </Text>
  );

  return (
    <React.Fragment>
      <Layout
        level='1'
        {...layoutProps}
        style={[styles.container, style]}>
        {hint && renderHintElement()}
        <Input category='s1' style={styles.input}>
          {value}
        </Input>
      </Layout>
      <Divider/>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input:{
    backgroundColor:'transparent',
    borderColor:'transparent'
  }
});
